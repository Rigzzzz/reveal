from django.http import HttpResponse
from django.shortcuts import render
from django.views import generic
from .models import Companies

# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the api index.")

def get(request, company_id):
    company = Companies.objects.get(id=company_id)

    return render(request, 'api/companies_detail.html', {
        'company': company
    })

class CompaniesListView(generic.ListView):
    model = Companies
    paginate_by = 20
    context_object_name = 'companies'

    def get_context_data(self, **kwargs):
        context = super(CompaniesListView,self).get_context_data(**kwargs)
        context['fields'] = context['companies'][0].get_fields()
        return context