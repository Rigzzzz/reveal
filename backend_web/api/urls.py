from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('companies/', views.CompaniesListView.as_view()),
    path('companies/<int:company_id>/', views.get, name='get'),
]